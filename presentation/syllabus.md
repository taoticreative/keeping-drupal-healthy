# Keeping Drupal Healthy

It's not enough to just get Drupal installed and create content; as a responsible site administrator, you also need to make sure you monitor the activity on your site and keep its code up-to-date. New releases of modules and Drupal core come out periodically, most of which fix problems, some of which add new whiz-bang features, and some of which address critical security problems.

## In this workshop we will:

* Learn about Drupal's "Watchdog" log and how to interpret and take action in response to the messages in it.
* Review Drupal and module version numbers and what they mean.
* Review the core Update Status module, and then discuss using maintenance mode, and the update.php script.


## Watchdog

* Watchdog is an error log that collects PHP errors and specific error messages sent to it by modules.
* If dblog is enabled, watchdog can be visited from the Reports section of your site. 'Admin > Reports > Recent log messages'
* Watchdog logs show the type, user whose action triggered them, the date and time they occured, location (the page that they were using that triggered the log, referrer (the page that they came to the location from, either on your site or externally), a message that indicates what may have happened and often debugging output, severity, hostname, and operations.
* Error levels: Notice, Warning, Debug, Info, Error, Critical, Alert, Emergency. These mirror PHP error levels.
* A log message in watchdog doesn't always indicate that something is broken or needs to be looked at. They are often used as routine indicators that a process took place.

### Exercise:
Find three issues with the Drupal installation

1. Go to 'Reports > Status report'
2. Find all lines that are yellow or red


## Status Report

* Drupal is dependant on the server ecosystem that it lives in.
* Issues with PHP, Apache, MySQL, Mail, and other servers can affect your sites performance and security.
* A lot of these kinds of issues can be diagnosed on the "Status Report" page.
* The Status Reports page also shows configuration issues, such as with the file system, insecure permissions on configuration files and sensitive directories
* It also displays whether various PHP libraries are present, which can affect whether certain contrib modules are able to perform their functions.

### Exercise:
Set the private files directory to one that actually exists

1. Go to 'Admin > Reports > Status Report'
2. Note the issues with the 'File System'
3. To fix this, go to 'Admin > Configuration > Media > File System'
4. The public path is fine, the private path does not exist. You should see an error message with a warning to this effect.
5. The correct path is 'sites/default/files/private' (It's not best practices for this to live here, but it is not inherently insecure, and this is easier for this demo)
6. Click 'Save Configuration'
7. Go back to the reports page and see that the File system lines are now green.


## Branch versions

* There are always two supported major versions of Drupal
* There is also the next release of Drupal that is being worked on
* Currently the versions are Drupal 7 and Drupal 6, with Drupal 8 in the works
* What supported means is that bugs and security holes are actively patched
* Using an unsupported version is highly insecure and not recommended
* When a new version of Drupal is released, the oldest supported version goes unsupported, so when Drupal 8 is released, Drupal 6 will no longer be supported
* The maintainers are giving Drupal 6 a three month grace period
* For Drupal 7 and previous versions, version numbers look like this: 7.x
* The 7 indicated the major version number (what functionality the release contains) and the x indicates the minor version number (bug and security fixes)

### Exercise:
Figure out what the most current release of Drupal is

1. Go to drupal.org
2. Click on 'Download and Extend'


## Module Upgrades

* For modules and themes, numbers have the form `CoreCompatibility-Major.PatchLevel[-Extra]`. For example, the media modules has the versions `7.x-1.4` and `7.x-2.0-alpha3`. Major versions of modules and themes can have very different functionality from one another.
* -dev, -alpha, -beta, rc: these suffixes indicate release status. Without a suffix indicates a stable release
* -dev is the branch work is currently being done on
* -alpha is a buggy version of the code with incomplete features
* -beta should contain most features but is still expected to be buggy
* -rc (release candidate) is a step up from beta, should have all functionality and is being tested as a potential stable release
* Generally you should use stable versions unless you really know what you are doing (and potential consequences)

### Exercise:
Find modules that are in need of updates

1. Go to 'Reports > Available Updates'
1. Find all lines that are red, yellow or gray


## Running Core or Module Upgrades

* Before doing any sort of upgrades, always make sure code and database are backed up
* Upgrades should be tested on a local site or development version of the site before attemped on a production site
* When running updates on a live site you must be logged in as User 1, or a user with 'Administer software updates' permission or you will not be able to run database updates
* Drupal provides something called 'Maintenance Mode' which shows a stripped down page that doesn't access the database, so that content won't be written to the database in the middle of an upgrade and potentially lost
* Update.php is a script that core provides which checks modules for database schema updates and then runs them, it's an important step when doing an update
* Update steps:
  * Do a backup
  * Log in as admin user
  * Put site in maintenance mode
  * Replace code for module or core system
  * Run update.php to do any database updates
  * Test!
  * Take site out of maintenance mode

* If things get really screwed up, there is a setting to allow any user,
including anonymous, to run a db update (but really this is what
your backup is for)

### Exercise:
Mini update scenario

1. Put site in maintenance mode
2. run update.php
3. then take site out of maintenance mode.

## Version control and Features

* The features module enables the capture and management of configuration
in Drupal.
* Features_UI module should be turned on for you to control states/features.
* Configure things like:
  * Variables: Site Name, Site Email
  * Entities: Content types, Fields
  * Display Modes, Image Styles
  * Views and Context
  * Roles and Permissions
* Most things in core are supported but the exception to this is blocks
  * You can work around this using Contexts (preferred)
  * Also a module called features_extras which gives limited block support.
    * This extra support is for core blocks. Blocks provided by contrib may
    have limited support.
* How features works, a brief rundown:
  * Drupal configuration is stored in a database. Depending on the object you
  store, it may look different.
  * Features takes these configurations and writes them into php.
  * Gives you a Features State where you can know whats in the database, and
  whats in code.
* Feature States
  * **Default** - The feature code and the database match. This means that
  the current configuration of your site match your code and there is no
  conflict.
  * **Overridden** - The database and code do not match. The database says
    ``$site_name = 'Our Wonderful Site';``, the database says
    ``$site_name = 'Our Improved Wonderful Site';``
    * In this situation the database is prioritzed so you and vistors would see
    Our Improved Wonderful Site.
  * **Rebuilding or Needs Review** - Similar to Overridden but used for
  'faux-exportables'. These are things related mostly to fields and content
  types where more complex operations are needed.
    * Rebuilding or Needs review **could** mean trouble ahead. Sometimes when
    changing field configuration while preserving data you need to do database
    alterations which require a level of understanding far out of scope.
    When dealing with needs review **always have a backup** and ** proceed
    with caution**. Sometimes it's an easy fix, sometimes it's going to be
    very complex.
    * As long as you don't change fields very often (things like displays and
      view modes are seperate from this) you will not see this often.
* Reverting a feature
  * So you have configured your website with features but have made a few
  changes. This is fairly standard, it should put your site in an
  **overridden** state. We have two options:
    1. To keep changes: **Revert**. This will tell the database
    replace it's current state with whats in code.
    2. We like the changes: **Rebuild**. This is a slightly more involved
    process but the short simple version is features will write new code for
    our module.
* Diff Module
 * Seeing states is great but not very helpful if Overridden. What's being
 overridden?
 * Diff is a module that may have used lets you see different revisions of
 content.
 * It also contains features plugins (many modules extend Diff).
 We can use Diff to see a 'diff' of our database compared to what we have said
 code or a feature. This will be very confusing at first with lots of extra
 details that you may not know what you need. Eventually you will learn how
 php arrays (how Drupal stores most of it's data) are written and it will
 be a bit less confusing.

### Exercise:
Revert a feature to fix broken settings

1. Find out what features in the site are overridden.
2. Find out which settings have been changed in each overridden feature.
3. Revert the overridden features.


## Backup and Migrate

* You WILL screw up. Backups will save your ass.
* Backup and migrate is a module that allows backups to be made through the website. It also allows the site to be restored from those backups.
* Your hosting provider may run backups of the server, but it's still important to keep separate backups of the site, depending on the schedule of backups, it really depends on how much data you are willing to lose if something goes wrong.
* Backup and migrate can also run backups on a regular schedule, and decide how many backups you'd like to keep.
* It keeps backups in a tar file by default, it can also be configured to output zip or other formats.
* It's very important to test your backups periodically to ensure that they are going properly, and that the site can actually be fully restored from them.

### Exercise:
Configure backup and migrate.
Run a backup.

1. Go to 'Admin > Configuration > System > Backup and Migrate'
2. To do a backup quickly, the Quick Backup settings are fine.
3. Choose 'Back up my Entire site (code, files & DB)' using default settings.
4. Click Backup Now. This should save a tarball to your default downloads directory on your computer.
5. There are more options under 'Advanced Options' that can be set for schedule backups.
6. Under 'Advanced Options'
7. Under the Schedule tab, click 'Add schedule'
8. Choose 'Scheduled Backups Directory' for Backup Destination, and 'Backup every 1 day(s) for frequency'

We won't actually run through a restore operation, due to potential complications, when doing an actual restore ALWAYS run it on a test site first.


 
